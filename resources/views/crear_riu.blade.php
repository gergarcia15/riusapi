<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <h1>Creació de rius</h1>
                <br/><br/>
                <form action="/rius" method="POST">
                @method('POST')
                @csrf
                    <input type="text" name="nom" id="nom" placeholder="nom">
                    <br/><br/>
                    <input type="number" name="longitud" id="longitud" placeholder="longitud (en m)">
                    <br/><br/><br/>
                    <input type="submit" value="Agregar Riu">
                </form>
            </div>
        </div>
    </body>
</html>
