<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h2>Llista de lectures</h2>
        <br/>
        <table>
            <thead>
            <tr>
                <th><h4>Id - Sensor</h4></th>
                <th><h4>Dia</h4></th>
                <th><h4>Hora</h4></th>
                <th><h4>Temperatura (Max)</h4></th>
            </tr>
            <br/>
            </thead>

            <tbody>
            @foreach($lecturasMaximes as $lectura )
                    <tr><td>{{$lectura->id}}-{{$lectura->sensor_id}}</td><td>{{$lectura->dia}}</td><td>{{$lectura->hora}}:00</td><td>{{$lectura->temperatura}} ºC</td></tr>
            @endforeach
            </tbody>
        </table>
        <br/><br/>
        <a href="/lecturas">Llista de lectures</a>
        <br/><br/>
        <a href="/">Pàgina Inicial</a>
        <a href="/lecturas/create">Crear nova lectura</a>

    </div>
</div>
</body>

</html>