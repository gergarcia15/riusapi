<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <form action="/lecturas" method="POST">
                    @method('POST')
                    @csrf
                    <select name="idSensor" id="idSensor">
                        @foreach($sensor_llista as $sensor)
                            <option value="{{$sensor->id}}">Riu-{{$sensor->riu_id}} Ubicació-({{$sensor->ubicacio}})</option>
                        @endforeach
                    </select>
                    <input type="date" name="dia" id="dia" placeholder="Dia">
                    <input type="number" min="0" max="23" name="hora" id="hora" placeholder="Hora">
                    <input type="number" name="temperatura" id="temperatura" placeholder="Temperatura">
                    <input type="submit" value="Agregar sensor">
                </form>
            </div>
        </div>
    </body>
</html>
