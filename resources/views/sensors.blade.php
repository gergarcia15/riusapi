<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h2>Llista de sensors</h2>
        <br/>
        <table>
            <thead>
            <tr>
                <th><h4>Sensor-Riu</h4></th>
                <th><h4>Ubicació</h4></th>
                <th><h4>Marca</h4></th>
                <th><h4>Model</h4></th>
                <th></th>
            </tr>
            <br/>
            </thead>

            <tbody>
            @foreach($sensors as $sensor)
                <tr>
                    <th>{{$sensor->id}} - {{$sensor->riu_id}}</th>
                    <th>{{$sensor->ubicacio}}</th>
                    <th>{{$sensor->marca}}</th>
                    <th>{{$sensor->model}}</th>
                    <th><a href="sensors/{{$sensor->id}}/edit"><button>Editar</button></a></th>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br/><br/>
        <a href="/maxNumberData">Màxim nº lectures</a>
        <br/><br/>
        <a href="/">Pàgina Inicial</a>
        <a href="/sensors/create">Crear nou sensor</a>

    </div>
</div>
</body>
</html>