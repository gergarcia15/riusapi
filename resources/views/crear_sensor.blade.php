<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

    </head>
    <body>
    <div class="flex-center position-ref full-height">
            <div class="content">
                <h1>Creació sensors</h1>
                <form action="/sensors" method="POST">
                @method('POST')
                @csrf
                    <select name="idRiu" id="idRiu">
                        @foreach($riu_llista as $riu)
                            <option value="{{$riu->id}}">{{$riu->nom}}</option>
                        @endforeach
                    </select>
                    <input type="text" name="ubicacio" id="ubicacio" placeholder="Ubicació">
                    <input type="text" name="marca" id="marca" placeholder="Marca">
                    <input type="text" name="model" id="model" placeholder="Model">
                    <input type="submit" value="Agregar sensor">
                </form>
            </div>
        </div>
    </body>
</html>
