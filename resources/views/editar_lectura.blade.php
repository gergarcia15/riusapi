<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

    </head>
    <body>
    <div class="flex-center position-ref full-height">
            <div class="content">
                <h2>Editar lectura</h2>
                <form action="/lecturas/{{$lecturaEspecific->id}}" method="POST">
                @method('PATCH')
                @csrf
                    <input type="date" name="dia" id="dia" value="{{$lecturaEspecific->dia}}">
                    <input type="number" min="0" max="23" name="hora" id="hora" value="{{$lecturaEspecific->hora}}">
                    <input type="number" name="temperatura" id="temperatura" value="{{$lecturaEspecific->temperatura}}">
                    <input type="submit" value="Actualitzar sensor">
                </form>
            </div>
        </div>
    </body>
</html>
