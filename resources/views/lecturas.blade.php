<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
       <h2>Llista de lectures</h2>
        <br/>
        <table>
            <thead>
            <tr>
                <th><h4>Id - Sensor</h4></th>
                <th><h4>Dia</h4></th>
                <th><h4>Hora</h4></th>
                <th><h4>Temperatura</h4></th>
                <th></th>
            </tr>
            <br/>
            </thead>

            <tbody>
            @foreach($lecturas as $lectura)
                <tr>
                    <th>{{$lectura->id}} - {{$lectura->sensor_id}}</th>
                    <th>{{$lectura->dia}}</th>
                    <th>{{$lectura->hora}}:00</th>
                    <th>{{$lectura->temperatura}} ºC</th>
                    <th><a href="lecturas/{{$lectura->id}}/edit"><button>Editar</button></a></th>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br/><br/>
        <a href="/maxTemp">Màxima temperatura</a>
        <br/><br/>
        <a href="/">Pàgina Inicial</a>
        <a href="/lecturas/create">Crear nova lectura</a>

    </div>
</div>
</body>
</html>