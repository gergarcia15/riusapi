<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

    </head>
    <body>
    <div class="flex-center position-ref full-height">
            <div class="content">
                <h2>Editar riu</h2>
                <form action="/rius/{{$riuEspecific->id}}" method="POST">
                @method('PATCH')
                @csrf
                    <input type="text" name="nom" id="nom" value="{{$riuEspecific->nom}}">
                    <input type="number" name="longitud" id="longitud" value="{{$riuEspecific->longitud}}">
                    <input type="submit" value="Actualitzar riu">
                </form>
            </div>
        </div>
    </body>
</html>
