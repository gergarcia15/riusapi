<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h2>Llista de rius</h2>
        <br/>
        <table>
            <thead>
                <tr>
                    <th><h4>Nom</h4></th>
                    <th><h4>Longitud</h4></th>
                    <th></th>
                </tr>
                <br/>
            </thead>

            <tbody>
            @foreach($rius as $riu)
                <tr>
                    <th>{{$riu->nom}}</th>
                    <th>{{$riu->longitud}} m</th>
                    <th><a href="rius/{{$riu->id}}/edit"><button>Editar</button></a></th>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br/><br/>
        <a href="/maxEstacions">Màximes estacions</a>
        <br/><br/>
        <a href="/">Pàgina Inicial</a>
        <a href="/rius/create">Crear nou riu</a>

    </div>
</div>
</body>
</html>