<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

    </head>
    <body>
    <div class="flex-center position-ref full-height">
            <div class="content">
                <h2>Editar sensor</h2>
                <form action="/sensors/{{$sensorEspecific->id}}" method="POST">
                @method('PATCH')
                @csrf
                    <input type="text" name="ubicacio" id="ubicacio" value="{{$sensorEspecific->ubicacio}}">>
                    <input type="text" name="marca" id="marca" value="{{$sensorEspecific->marca}}">>
                    <input type="text" name="model" id="model" value="{{$sensorEspecific->model}}">>
                    <input type="submit" value="Actualitzar sensor">
                </form>
            </div>
        </div>
    </body>
</html>
