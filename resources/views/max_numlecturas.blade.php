<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/estils.css')}}">

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h2>Llista de sensors</h2>
        <br/>
        <table>
            <thead>
            <tr>
                <th><h4>Sensor-Riu</h4></th>
                <th><h4>Ubicació</h4></th>
                <th><h4>Marca</h4></th>
                <th><h4>Nº de lectures</h4></th>
                <th></th>
            </tr>
            <br/>
            </thead>

            <tbody>
            @foreach(array_keys($sensorMaxes) as $llave )
                @if($sensorMaxes[$llave]==$numeroMax)
                    <tr><td>{{$sensores[$llave]->id}} - {{$sensores[$llave]->riu_id}}</td><td>{{$sensores[$llave]->ubicacio}}</td><td>{{$sensores[$llave]->marca}} m</td><td>{{$numeroMax}}</td></tr>
                @endif
            @endforeach
            </tbody>
        </table>
        <br/><br/>
        <a href="/sensors">Llista de sensors</a>
        <br/><br/>
        <a href="/">Pàgina Inicial</a>
        <a href="/sensors/create">Crear nou sensor</a>

    </div>
</div>
</body>

</html>