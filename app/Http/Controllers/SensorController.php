<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Riu;
use App\Sensor;
use App\Lectura;

class SensorController extends Controller
{
    public function index()
    {
        $sensors = Sensor::all();
        return view('sensors', ["sensors"=> $sensors]); // blade_file_name => nombre de la vista de listado de rius

        //
    }

    //Show the form for creating a new resource.

    public function create()
    {
        $riuNomMap = Riu::all("id","nom");
        return view('crear_sensor', ["riu_llista"=> $riuNomMap]);
    }


    //Store a newly created resource in storage.

    public function store(Request $request)
    {
        $sensor = new Sensor;
        $sensor->ubicacio = $request->ubicacio;
        $sensor->marca = $request->marca;
        $sensor->model = $request->model;
        $sensor->riu_id = $request->idRiu;
        $sensor->save();
        return redirect('/sensors');
    }


    //Display the specified resource.

    public function show($id)
    {
        return redirect('/sensors');
    }


    //Show the form for editing the specified resource.

    public function edit($id)
    {
        $sensor_especific = Sensor::where("id", $id)->first();
        return view('editar_sensor',["sensorEspecific"=> $sensor_especific]);
    }


    //Update the specified resource in storage.

    public function update(Request $request, $id)
    {
        //Lo mismo que "store" solo que en lugar de "$sensor = new Sensor ;" será $sensor = Sensor::where("id", 1)

        $sensor = Sensor::find($id);
        $sensor->ubicacio = $request->ubicacio;
        $sensor->marca = $request->marca;
        $sensor->model = $request->model;
        $sensor->save();
        return redirect('/sensors');
    }


    //Remove the specified resource from storage.

    public function destroy($id)
    {
        $sensor = Sensor::where("id", $id)->destroy();
    }

    public function maxNumberData(){
        $sensorIdMax=DB::select("SELECT SubSelect1.mycount stationCount, SubSelect1.sensor_id FROM ( SELECT sensor_id, COUNT(id) mycount FROM lecturas GROUP BY sensor_id)AS SubSelect1");
        $sensorIdList  = array();
        $sensorList  = array();
        foreach ($sensorIdMax as $sensorId){
            $sensorIdList[$sensorId->sensor_id]=($sensorId->stationCount); // ID's de rio en la llave, su cantidad en el valor
        }

        $sensorMax=max($sensorIdList);

        $sensors = Sensor::findMany(array_keys($sensorIdList));
        $arrayKeys=array_keys($sensorIdList);
        $int =0;
        foreach ($sensors as $sensor){

            echo($sensor->sensor_id);
            $sensorList[$arrayKeys[$int]]=$sensor; // ID's de rio en la llave, su cantidad en el valor
            $int++;
        }
        return view('max_numlecturas', ["sensores"=> $sensorList,"numeroMax"=>$sensorMax, "sensorMaxes"=>$sensorIdList]);
    }
}
