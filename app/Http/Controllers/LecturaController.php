<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Riu;
use App\Lectura;
use App\Sensor;

class LecturaController extends Controller
{
    public function index()
    {
        $lecturas = Lectura::all();
        return view('lecturas', ["lecturas"=> $lecturas]); // blade_file_name => nombre de la vista de listado de rius

        //
    }

    //Show the form for creating a new resource.

    public function create()
    {
        $sensorNomMap = Sensor::all("id","riu_id","ubicacio");
        return view('crear_lectura', ["sensor_llista"=> $sensorNomMap]);

    }


    //Store a newly created resource in storage.

    public function store(Request $request)
    {
        $lectura = new Lectura;
        $lectura->dia = $request->dia;
        $lectura->hora = $request->hora;
        $lectura->temperatura = $request->temperatura;
        $lectura->sensor_id = $request->idSensor;
        $lectura->save();
        return redirect('/lecturas');
    }

    //Display the specified resource.

    public function show($id)
    {
        return redirect('/lecturas');

    }


    //Show the form for editing the specified resource.

    public function edit($id)
    {
        $lectura_especific = Lectura::where("id", $id)->first();
        return view('editar_lectura',["lecturaEspecific"=> $lectura_especific]);
    }


    //Update the specified resource in storage.

    public function update(Request $request, $id)
    {
        //Lo mismo que "store" solo que en lugar de "$lectura = new Lectura ;" será $lectura = Lectura::where("id", 1)

        $lectura = Lectura::find($id);
        $lectura->dia = $request->dia;
        $lectura->hora = $request->hora;
        $lectura->temperatura = $request->temperatura;
        $lectura->save();
        return redirect('/lecturas');

    }


    //Remove the specified resource from storage.

    public function destroy($id)
    {
        $lectura = Lectura::where("id", $id)->destroy();
    }

    public function maxTemp(){
        $lecturaMaxTemp=Lectura::where("temperatura", Lectura::max("temperatura"))->get();
        return view('max_lecturas', ["lecturasMaximes"=> $lecturaMaxTemp]);
    }

}

