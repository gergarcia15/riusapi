<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Riu;
use App\Lectura;
use App\Sensor;

class RiuController extends Controller
{
    public function index()
    {
        $rius = Riu::all();
        return view('rius', ["rius"=> $rius]); // blade_file_name => nombre de la vista de listado de rius

        //
    }

    //Show the form for creating a new resource.

    public function create()
    {
            return view('crear_riu');
    }


    //Store a newly created resource in storage.

    public function store(Request $request)
    {
        $riu = new Riu;
        $riu->nom = $request->nom;
        $riu->longitud = $request->longitud;
        $riu->save();
        return redirect('/rius');
    }


    //Display the specified resource.

    public function show($id)
    {
        return redirect('/rius');
    }


    //Show the form for editing the specified resource.

    public function edit($id)
    {
        $riu_especific = Riu::where("id", $id)->first();
        return view('editar_riu',["riuEspecific"=> $riu_especific]);
    }


    //Update the specified resource in storage.

    public function update(Request $request, $id)
    {
        //Lo mismo que "store" solo que en lugar de "$riu = new Riu ;" será $riu = Riu::where("id", 1)
        $riu = Riu::find($id);
        $riu->nom = $request->nom;
        $riu->longitud = $request->longitud;
        $riu->save();
        return redirect('/rius');
    }


    //Remove the specified resource from storage.

    public function destroy($id)
    {
        $riu = Riu::where("id", $id)->destroy();
    }

    public function maxStations(){
        $riuIdMax=DB::select("SELECT SubSelect1.mycount stationCount, SubSelect1.riu_id FROM ( SELECT riu_id, COUNT(id) mycount FROM sensors GROUP BY riu_id)AS SubSelect1");
        $riuIdList  = array();
        $riuList  = array();
        foreach ($riuIdMax as $riuId){
            $riuIdList[$riuId->riu_id]=($riuId->stationCount); // ID's de rio en la llave, su cantidad en el valor
        }

        $riuMax=max($riuIdList);

        $rius = Riu::findMany(array_keys($riuIdList));
        $arrayKeys=array_keys($riuIdList);
        $int =0;
        foreach ($rius as $riu){

            echo($riu->riu_id);
            $riuList[$arrayKeys[$int]]=$riu; // ID's de rio en la llave, su cantidad en el valor
            $int++;
        }
        return view('max_stations', ["rios"=> $riuList,"numeroMax"=>$riuMax, "riuMaxes"=>$riuIdList]);
    }

}
